# Volcano and population interactive web map
* [Subject](#subject)
* [Covered topics](#covered-topics)
* [Libraries](#libraries)

# Subject   :pushpin:
The goal is to create an interactive map showing :
- Volcanoes on the western coast of the USA
- The world population

# Covered topics
- Jupyter notebook
- Anonymous method (lambda)
- Fetch online data (maps and locations)
- GeoJson
- Toogling between map features
- Generation of a HTML page

# Libraries :books:
- pandas
- folium

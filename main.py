import folium
import pandas

def color_select(elevation):
    if elevation < 1000:
        return "green"
    elif elevation >= 1000 and elevation < 3000:
        return "orange"
    else:
        return "red"


if __name__ == '__main__':
    # Fetch data
    data = pandas.read_csv("ressources/Volcanoes.txt")

    # Create a map
    map = folium.Map(location=[38.58, -99.09], zoom_start = 6, tiles = "Stamen Terrain")

    # Add HTML formatting
    html = """<h4>Volcano informations:</h4>
    <p>Name : %s</p>
    <p>Height: %s m</p>
    """

    # Add objects on the map with a feature group
    feature_group_vol = folium.FeatureGroup("Volcanoes")
    feature_group_pop = folium.FeatureGroup("Population")

    # Create markers with folium
    # Do not forget zip() to handle several lists
    for name, lat, lon, elv in zip(data.NAME, data.LAT, data.LON, data.ELEV):
        iframe = folium.IFrame(html=html % (name, str(elv)), width=200, height=100)
        feature_group_vol.add_child(folium.Marker(location=[lat, lon],
                                                  popup=folium.Popup(iframe),
                                                  icon=folium.Icon(color_select(elv))))

    # Add a GeoJson file to represent population by country
    world_pop_data = open("ressources/world.json", mode='r', encoding='utf-8-sig').read()

    feature_group_pop.add_child(folium.GeoJson(data=world_pop_data,
                                               ## Anonymous function
                                               style_function=lambda x : {'fillColor':'green' if x['properties']['POP2005'] < 10000000
                                               else 'orange' if x['properties']['POP2005'] < 20000000
                                               else 'red'}))
    # Add features to map
    map.add_childs(feature_group_vol, feature_group_pop)

    # Setup layer control on map [!! It needs to be added AFTER the feature group!!]
    # Appears at the upper right corner of the map
    map.add_child(folium.LayerControl())

    # Save html map
    map.save("ressources/Volcanoes_map.html")